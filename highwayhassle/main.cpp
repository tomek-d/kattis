#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <climits>

using namespace std;

typedef unsigned long long cost;

struct Edge {
  int nodeA;
  int nodeB;
  int weight;

  Edge(int na, int nb, int w): nodeA(na), nodeB(nb), weight(w) {}

  bool operator < (const Edge& other) const { return weight < other.weight; }
};
typedef struct Edge Edge;

struct Node {
  int id;
  int fuelCost;
  //vector<Edge> edges;
  map<int, int> edges;

  Node(int i): id(i) {}
};
typedef struct Node Node;

class Problem {
  int nCount;
  int eCount;
  int sCount;

  int start, end;
  int tankCapacity;

  Node** nodes;

public:

  void readInput() {
    cin >> nCount >> eCount >> sCount;
    cin >> tankCapacity;

    nodes = new Node*[nCount];
    // init nodes
    for (int i = 0; i < nCount; ++i)
      nodes[i] = new Node(i);

    //read edges
    int na, nb, ncost;
    for (int i = 0; i < eCount; ++i) {
      cin >> na >> nb >> ncost;
      --na, --nb;

      nodes[na] -> edges.insert(pair<int,int>(nb, ncost));
      nodes[nb] -> edges.insert(pair<int,int>(na, ncost));
      // nodes[na] -> edges.push_back(Edge(na, nb, ncost));
      // nodes[nb] -> edges.push_back(Edge(nb, na, ncost));
    }

    //read petrol stations
    for (int i = 0; i < sCount; ++i) {
      cin >> na >> ncost;
      --na;
      nodes[na] -> fuelCost = ncost;
    }

    cin >> start >> end;
    --start, --end;
  }

  bool canBeReduced(Node* n) {
    return n -> fuelCost == 0
        && n -> id != end;
  }

  void reduceNodes() {
    //Lets remove unnecessary node
    for (int i = 0; i < nCount; ++i) {
      Node* node = nodes[i];
      if (canBeReduced(node)) {
        const map<int, int>& edges = node -> edges;
        for (auto it1 = edges.begin(); it1 != edges.end(); ++it1) {
          auto itm = nodes[it1 -> first] -> edges.find(node -> id);
          for (auto it2 = edges.begin(); it2 != edges.end(); ++it2) {
            if (it1 != it2) {
              auto itn = nodes[it2 -> first] -> edges.find(node -> id);
              int cost = itm -> second + itn -> second;
              if (cost <= tankCapacity) {
                nodes[it1 -> first] -> edges.insert(pair<int, int>(it2 -> first, cost));
                nodes[it2 -> first] -> edges.insert(pair<int, int>(it1 -> first, cost));
              }
            }
          }
        }
        for (auto it1 = edges.begin(); it1 != edges.end(); ++it1) {
          nodes[it1 -> first] -> edges.erase(node -> id);
        }
      }
    }
  }

  void find_reachable(const int node, int cost) {

  }

  cost try_edge(const Edge& edge, const set<pair<int,int>> visited, const priority_queue<Edge>& toBeVisited, cost totalCost, int fuelLeft) {
    return 0;
  }

  int solve() {

    set<pair<int,int>> visited;
    priority_queue<Edge> toBeVisited;
    Node* node = nodes[start];
    for (auto it = node -> edges.begin(); it != node -> edges.end(); ++it) {
      toBeVisited.push(Edge(node -> id, it -> first, it -> second));
    }

    cost cost = ULONG_LONG_MAX;
    while (!toBeVisited.empty()) {
      const Edge& e = toBeVisited.top();
      toBeVisited.pop();

      cost = min(try_edge(e, visited, toBeVisited, 0L, 0), cost);
      cost = min(try_edge(e, visited, toBeVisited, tankCapacity * e.weight, tankCapacity), cost);

    }

    return cost;
  }
};

int main() {

  int cases;
  cin >> cases;

  for (int i = 0; i < cases; ++i) {
    Problem p;
    p.readInput();
    p.reduceNodes();
    cout << p.solve() << endl;
  }

  return 0;
}
