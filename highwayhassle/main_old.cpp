#include <iostream>
#include <vector>
#include <set>
#include <queue>
#include <climits>

using namespace std;

struct SVisited {
  int node;
  int totalCost;
  int fuelLeft;
  set<int> visited;
  SVisited(int n, int t, int f, set<int> v):node(n), totalCost(t), fuelLeft(f), visited(v) {}
};
typedef SVisited Visited;

struct CmpCosts {
  bool operator() (const Visited& e1, const Visited& e2) const {
    return e1.totalCost < e2.totalCost;
  }
};

typedef priority_queue<Visited, vector<Visited>, CmpCosts> ReachablesQueue;

struct SEdge{
  int source;
  int target;
  int cost;

  SEdge(int s, int t, int c) : source(s), target(t), cost(c) {}
};
typedef SEdge Edge;

struct SNode {
  int id;
  int fuelCost;
  vector<Edge> connections;
};
typedef SNode Node;

void get_reachable_nodes(Node* nodes[], int n, int end, int fuel, int fcost, int total, int max_cap, set<int>& visited, ReachablesQueue& reachable) {
  Node* curr = nodes[n];

  for (int i = 0; i < curr->connections.size(); ++i) {
    Edge& e = curr->connections[i];
    if (e.cost + fuel <= max_cap && visited.find(e.target) == visited.end()) {
      set<int> vClone(visited.begin(), visited.end());
      vClone.insert(e.target);

      if (e.target == end) {
        reachable.push(Visited(e.target, total + fcost * (fuel + e.cost), max_cap - (fuel + e.cost), vClone));
      } else if (nodes[e.target]->fuelCost > 0) {
        reachable.push(Visited(e.target, total + fcost * (fuel + e.cost), max_cap - (fuel + e.cost), vClone));
        get_reachable_nodes(nodes, e.target, end, fuel + e.cost, fcost, total, max_cap, vClone, reachable);
      }
    }
  }
}

void reduceNodes(Node* nodes[], int ncount, int start, int end) {
  //reduce all nodes that are not start, end or petrol station
  //how?
  //do not delete anything - thats not a point, just update edge!

  vector<Node*> nonPetrol;

  nonPetrol.reserve(ncount);

  for (int i = 0; i < ncount; ++i) {
    if (nodes[i] -> fuelCost == 0 && i != start && i != end)
      nonPetrol.push_back(nodes[i]);
  }

  for (int i = 0; i < nonPetrol.size(); ++i) {
    set<int> neigh;
    Node* ref = nonPetrol[i];
    for (int j = 0; j < ref -> connections.size(); ++j) {
      neigh.insert(ref -> connections[j].source);
      neigh.insert(ref -> connections[j].target);
    }
  //  neigh.erase()
  }

}
int solveCase() {
  int n, e, s;
  int maxFuel;
  cin >> n >> e >> s;
  cin >> maxFuel;

  Node* nodes[n];
  for (int i = 0; i < n; ++i) {
    nodes[i] = new Node();
    nodes[i] -> id = i;
  }

  for (int i = 0; i < e; ++i) {
    int a, b, fuel;
    cin >> a >> b >> fuel;
    --a; --b;
    nodes[a] -> connections.push_back(Edge(a, b, fuel));
    nodes[b] -> connections.push_back(Edge(b, a, fuel));
  }

  for (int i = 0; i < s; ++i) {
    int node, cost;
    cin >> node >> cost;
    nodes[node - 1]->fuelCost = cost;
  }

  int start, end;
  cin >> start >> end;
  --start, --end;

  ReachablesQueue reachables;
  reachables.push(Visited(start, 0, maxFuel, set<int>()));

  int min_cost = INT_MAX;
  while (!reachables.empty()) {
    Visited curr = reachables.top();
    reachables.pop();
    if (curr.node == end) {
      min_cost = min(min_cost, curr.totalCost);
    } else {
      set<int> vClone(curr.visited.begin(), curr.visited.end());
      vClone.insert(curr.node);
      get_reachable_nodes(nodes, curr.node, end, 0, nodes[curr.node]->fuelCost , curr.totalCost, maxFuel, vClone, reachables);
    }
  }

  return min_cost;
}

int main() {

  int cases;
  cin >> cases;

  for (int i = 0; i < cases; ++i) {
    cout << solveCase() << endl;
  }

  return 0;
}
