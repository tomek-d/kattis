#include <iostream>
#include <vector>
#include <cstring>
#include <queue>
#include <algorithm>
#include <map>

using namespace std;

const int MAX_INPUT = 3000;

struct Node {
  vector<Node*> subNodes;
  Node* parent = NULL;
  int weight = 0;
  int hash() {
    return subNodes.size() * 37 ^ weight;
  }
};

bool cmpNodes(Node* n1, Node* n2) {
  if (n1->hash() == n2->hash()) {
    if (n1->subNodes.size() == n2->subNodes.size())
      return n1->weight < n2->weight;
    return n1->subNodes.size() < n2->subNodes.size();
  }
  return false;
}

Node* buildTree(char* line) {
  Node* root = new Node();
  Node* current = root;
  vector<Node*> nodes;
  nodes.reserve(MAX_INPUT / 2);
  nodes.push_back(root);

  int len = strlen(line);
  for (int i = 0; i < len; ++i) {
    if (line[i] == '0') {
      //construct new Node
      Node* newNode = new Node();
      newNode->parent = current;
      nodes.push_back(newNode);

      current->subNodes.push_back(newNode);
      current = newNode;

    } else {
      //go back
      current = current->parent;
    }
  }

  for (auto it = nodes.begin(); it != nodes.end(); ++it) {
    Node* node = *it;
    while ((node = node->parent) != NULL) ++node->weight;
  }

  return root;
}

bool compNodes(Node* n1, Node* n2) {
  vector<Node*> &s1 = n1->subNodes;
  vector<Node*> &s2 = n2->subNodes;

  if (s1.size() != s2.size() || n1->weight != n2->weight)
    return false;

  map<int, vector<Node*>> q;

  for (int i = 0; i < s2.size(); ++i) {
    auto found = q.find(s2[i]->subNodes.size());
    if(found == q.end()) {
      vector<Node*> tmp;
      tmp.push_back(s2[i]);
      q.insert(pair<int, vector<Node*>>(s2[i]->subNodes.size(), tmp));
    } else {
      found->second.push_back(s2[i]);
    }
  }

  for (int i = 0; i < s1.size(); ++i) {
    auto fq =  q.find(s1[i]->subNodes.size());
    if (fq == q.end()) return false;

    bool matches = false;
    for(int j = 0; j < fq->second.size(); ++j) {
      if (compNodes(s1[i], fq->second[j])) {
        fq->second.erase(fq->second.begin() + j);
        matches = true;
        break;
      }
    }
    if(!matches) return false;
  }
  int left = 0;
  for(auto it = q.begin(); it != q.end(); ++it) {
    left += it->second.size();
  }

  return left == 0;
}

int main() {

  int n;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    char line1[MAX_INPUT];
    char line2[MAX_INPUT];
    cin >> line1;
    cin >> line2;
    Node* l1 = buildTree(line1);
    Node* l2 = buildTree(line2);

    bool areSame = compNodes(l1, l2);
    cout << (areSame ? "same" : "different") << endl;
  }

  return 0;
}
